/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/urock/msu_project_01/msu_project_01.srcs/sources_1/imports/files/adder_tb_self_check.vhd";
extern char *IEEE_P_2592010699;
extern char *IEEE_P_3499444699;
extern char *IEEE_P_3620187407;

char *ieee_p_3499444699_sub_2213602152_3536714472(char *, char *, int , int );
unsigned char ieee_p_3620187407_sub_4042748798_3965413181(char *, char *, char *, char *, char *);


static void work_a_1212305296_2372691052_p_0(char *t0)
{
    char t4[16];
    char *t1;
    char *t2;
    char *t3;
    char *t5;
    char *t6;
    char *t7;
    unsigned int t8;
    unsigned char t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;

LAB0:    xsi_set_current_line(44, ng0);

LAB3:    t1 = (t0 + 1512U);
    t2 = *((char **)t1);
    t1 = (t0 + 1352U);
    t3 = *((char **)t1);
    t5 = ((IEEE_P_2592010699) + 4024);
    t6 = (t0 + 6124U);
    t7 = (t0 + 6124U);
    t1 = xsi_base_array_concat(t1, t4, t5, (char)97, t2, t6, (char)97, t3, t7, (char)101);
    t8 = (4U + 4U);
    t9 = (8U != t8);
    if (t9 == 1)
        goto LAB5;

LAB6:    t10 = (t0 + 3480);
    t11 = (t10 + 56U);
    t12 = *((char **)t11);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t1, 8U);
    xsi_driver_first_trans_fast(t10);

LAB2:    t15 = (t0 + 3400);
    *((int *)t15) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(8U, t8, 0);
    goto LAB6;

}

static void work_a_1212305296_2372691052_p_1(char *t0)
{
    char t11[16];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    int t7;
    int t8;
    int t9;
    int t10;
    char *t12;
    char *t13;
    unsigned int t14;
    unsigned char t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    int64 t21;
    int t22;
    int t23;
    int t24;

LAB0:    t1 = (t0 + 3080U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(52, ng0);
    t2 = (t0 + 3544);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(54, ng0);
    t2 = (t0 + 6173);
    *((int *)t2) = 0;
    t3 = (t0 + 6177);
    *((int *)t3) = 15;
    t7 = 0;
    t8 = 15;

LAB4:    if (t7 <= t8)
        goto LAB5;

LAB7:    xsi_set_current_line(79, ng0);

LAB29:    *((char **)t1) = &&LAB30;

LAB1:    return;
LAB5:    xsi_set_current_line(56, ng0);
    t4 = (t0 + 6181);
    *((int *)t4) = 0;
    t5 = (t0 + 6185);
    *((int *)t5) = 15;
    t9 = 0;
    t10 = 15;

LAB8:    if (t9 <= t10)
        goto LAB9;

LAB11:
LAB6:    t2 = (t0 + 6173);
    t7 = *((int *)t2);
    t3 = (t0 + 6177);
    t8 = *((int *)t3);
    if (t7 == t8)
        goto LAB7;

LAB26:    t9 = (t7 + 1);
    t7 = t9;
    t4 = (t0 + 6173);
    *((int *)t4) = t7;
    goto LAB4;

LAB9:    xsi_set_current_line(58, ng0);
    t6 = (t0 + 6173);
    t12 = ieee_p_3499444699_sub_2213602152_3536714472(IEEE_P_3499444699, t11, *((int *)t6), 4);
    t13 = (t11 + 12U);
    t14 = *((unsigned int *)t13);
    t14 = (t14 * 1U);
    t15 = (4U != t14);
    if (t15 == 1)
        goto LAB12;

LAB13:    t16 = (t0 + 3608);
    t17 = (t16 + 56U);
    t18 = *((char **)t17);
    t19 = (t18 + 56U);
    t20 = *((char **)t19);
    memcpy(t20, t12, 4U);
    xsi_driver_first_trans_fast(t16);
    xsi_set_current_line(59, ng0);
    t2 = (t0 + 6181);
    t3 = ieee_p_3499444699_sub_2213602152_3536714472(IEEE_P_3499444699, t11, *((int *)t2), 4);
    t4 = (t11 + 12U);
    t14 = *((unsigned int *)t4);
    t14 = (t14 * 1U);
    t15 = (4U != t14);
    if (t15 == 1)
        goto LAB14;

LAB15:    t5 = (t0 + 3672);
    t6 = (t5 + 56U);
    t12 = *((char **)t6);
    t13 = (t12 + 56U);
    t16 = *((char **)t13);
    memcpy(t16, t3, 4U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(61, ng0);
    t21 = (10 * 1000LL);
    t2 = (t0 + 2888);
    xsi_process_wait(t2, t21);

LAB18:    *((char **)t1) = &&LAB19;
    goto LAB1;

LAB10:    t2 = (t0 + 6181);
    t9 = *((int *)t2);
    t3 = (t0 + 6185);
    t10 = *((int *)t3);
    if (t9 == t10)
        goto LAB11;

LAB25:    t22 = (t9 + 1);
    t9 = t22;
    t4 = (t0 + 6181);
    *((int *)t4) = t9;
    goto LAB8;

LAB12:    xsi_size_not_matching(4U, t14, 0);
    goto LAB13;

LAB14:    xsi_size_not_matching(4U, t14, 0);
    goto LAB15;

LAB16:    xsi_set_current_line(63, ng0);
    t2 = (t0 + 1192U);
    t3 = *((char **)t2);
    t2 = (t0 + 6108U);
    t4 = (t0 + 6173);
    t5 = (t0 + 6181);
    t22 = *((int *)t4);
    t23 = *((int *)t5);
    t24 = (t22 + t23);
    t6 = ieee_p_3499444699_sub_2213602152_3536714472(IEEE_P_3499444699, t11, t24, 8);
    t15 = ieee_p_3620187407_sub_4042748798_3965413181(IEEE_P_3620187407, t3, t2, t6, t11);
    if (t15 != 0)
        goto LAB20;

LAB22:    xsi_set_current_line(66, ng0);
    t2 = (t0 + 3544);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);

LAB21:    xsi_set_current_line(69, ng0);
    t2 = (t0 + 1192U);
    t3 = *((char **)t2);
    t2 = (t0 + 6108U);
    t4 = (t0 + 6173);
    t5 = (t0 + 6181);
    t22 = *((int *)t4);
    t23 = *((int *)t5);
    t24 = (t22 + t23);
    t6 = ieee_p_3499444699_sub_2213602152_3536714472(IEEE_P_3499444699, t11, t24, 8);
    t15 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t3, t2, t6, t11);
    if (t15 == 0)
        goto LAB23;

LAB24:    goto LAB10;

LAB17:    goto LAB16;

LAB19:    goto LAB17;

LAB20:    xsi_set_current_line(64, ng0);
    t12 = (t0 + 3544);
    t13 = (t12 + 56U);
    t16 = *((char **)t13);
    t17 = (t16 + 56U);
    t18 = *((char **)t17);
    *((unsigned char *)t18) = (unsigned char)3;
    xsi_driver_first_trans_fast(t12);
    goto LAB21;

LAB23:    t12 = (t0 + 6189);
    xsi_report(t12, 6U, 2);
    goto LAB24;

LAB27:    goto LAB2;

LAB28:    goto LAB27;

LAB30:    goto LAB28;

}


extern void work_a_1212305296_2372691052_init()
{
	static char *pe[] = {(void *)work_a_1212305296_2372691052_p_0,(void *)work_a_1212305296_2372691052_p_1};
	xsi_register_didat("work_a_1212305296_2372691052", "isim/adder_tb2.exe.sim/work/a_1212305296_2372691052.didat");
	xsi_register_executes(pe);
}
