----------------------------------------------------------------------------------
-- Company: 		MSU
-- Engineer: 		urock
-- 
-- Create Date:    	22:47:17 10/27/2011 

-- Design Name: 		Atlys_study project
-- Module Name:    	half_adder - rtl 
-- Project Name: 		comb_02_adder

-- Description: 		

-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.ALL;


entity half_adder is
    Port ( a : in  std_logic;
           b : in  std_logic;
           s : out std_logic;
           c : out std_logic);
end half_adder;

architecture rtl of half_adder is

begin

	s <= a xor b;
	c <= a and b; 

end rtl;

