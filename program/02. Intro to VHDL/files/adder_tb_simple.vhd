--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:15:00 11/15/2011
-- Design Name:   
-- Module Name:   D:/projects/Atlys/ise/atlys_stydy_01/src/adder_top/adder_tb.vhd
-- Project Name:  atlys_stydy_01
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: adder_top
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY adder_tb IS
END adder_tb;
 
ARCHITECTURE behavior OF adder_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT adder_top
    PORT(
         switch_in : IN  std_logic_vector(7 downto 0);
         leds_out : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal switch_in : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal leds_out : std_logic_vector(7 downto 0);
	
	signal A, B	: std_logic_vector(3 downto 0); 

 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: adder_top PORT MAP (
          switch_in => switch_in,
          leds_out => leds_out
        );


	
	switch_in	<= B & A;
	
	

   -- Stimulus process
   stim_proc: process
   begin		
		
		A <= "0000";
		B <= "0000";
      wait for 100 ns;	

		A <= "0001";
		B <= "0100";
      wait for 100 ns;	

		A <= "0011";
		B <= "0101";
      wait for 100 ns;	

		A <= "0001";
		B <= "0100";
      wait for 100 ns;	

		A <= "0001";
		B <= "0111";
		

      wait;
   end process;

END;
