----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:37:03 10/01/2013 
-- Design Name: 
-- Module Name:    gray_distant - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description:  ����� �� �� - ����������� ��������� ����� ����� ���������� ���� ����
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;


entity gray_distant is
    Port ( a_g : in  STD_LOGIC_VECTOR (3 downto 0);
           b_g : in  STD_LOGIC_VECTOR (3 downto 0);
           distant : out  STD_LOGIC_VECTOR (3 downto 0));
end gray_distant;

architecture Behavioral of gray_distant is

	COMPONENT g_inc
	PORT(
		g : IN std_logic_vector(3 downto 0);          
		g_next : OUT std_logic_vector(3 downto 0)
		);
	END COMPONENT;
	
	
	type my_type is array (0 to 15) of std_logic_vector(3 downto 0); 
	signal a_arr	: my_type; 

begin



a_arr(0) <= a_g;

f1: for i in 0 to 14 generate
begin

		inc1: g_inc PORT MAP(
			g => a_arr(i),
			g_next => a_arr(i+1)
		);
end generate f1;
	
	
process(a_arr, b_g)
begin
	
	distant <= "0000"; 
	
	for i in 0 to 15 loop
		
		if a_arr(i) = b_g then
			
			distant <= conv_std_logic_vector(i,4); 
			
		end if;
	
	end loop;

end process;

end Behavioral;

