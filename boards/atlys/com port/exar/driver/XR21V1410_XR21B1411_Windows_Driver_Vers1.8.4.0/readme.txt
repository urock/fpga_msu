12/14/12

This driver is a custom driver for the XR21V1410 and XR21B1411.  This driver has been WHQL certified and have been tested on Windows 2000, XP, Vista and 7.  
The driver version is 1.8.4.0.

This is a special driver that will allow the XR21V1410 or XR21B1411 to be unplugged from the Windows PC and plugged back in while a test application is opened.  
After being plugged back in, the test application will work as if the USB UART was never unplugged from the system.  

This zip file contains the following items:

	xrusbser_1.8.4.0_x86 folder - 					Folder containing the 32-bit Windows driver
	xrusbser_1.8.4.0_x64 folder - 					Folder containing the 64-bit Windows driver
	XR21V1410_XR21B1411_Windows_Ver1840_x86_Installer.EXE - 	Automatic driver installer for 32-bit Windows driver
	XR21V1410_XR21B1411_Windows_Ver1840_x64_Installer.EXE - 	Automatic driver installer for 64-bit Windows driver

To install the driver with the driver installer, just double-click and run the EXE file and follow the instructions.  Then plug in the USB UART.

To install the driver with the driver folder, you will need to point to the directory where the driver is located during the installation process.  The installation
process may begin automatically when you plug in the USB UART.  If not, go to the Device Manager, open the properties page and click on "Update Driver".

Please send any technical questions to UARTTECHSUPPORT@EXAR.COM.  

Revision History (Rev 1.8.4.0) - December 2012
- Initial Release.  

